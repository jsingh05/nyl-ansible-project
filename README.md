# **New York Life NAS Provisioning Ansible Project**


This repository is designed to manage CIFS and NFS protocols for NAS provisioning. By using the playbook **nas.yml** a user can create or update the the CIFS and NFS shares, qtrees, quotas, and export policies(nfs only)

The documentation for the Ansible NetApp modules used for this project can be found at:

* https://docs.ansible.com/ansible/latest/collections/netapp/ontap/na_ontap_export_policy_rule_module.html
* https://docs.ansible.com/ansible/2.9_ja/modules/na_ontap_export_policy_module.html
* https://docs.ansible.com/ansible/latest/collections/netapp/ontap/na_ontap_quotas_module.html
* https://docs.ansible.com/ansible/latest/collections/netapp/ontap/na_ontap_qtree_module.html
* https://docs.ansible.com/ansible/latest/collections/netapp/ontap/na_ontap_export_policy_rule_module.html

## **Inventory Configuration**


The inventory for this ansible project is broke up into a dictionary inside the vars/main.yml file where you will provide the site, object_type(cifs or nfs), and env in the ansible tower survey or as extra vars. 

## Inventory Dictionary setup example in vars/main.yml:
This inventory is statically defined in the vars/main.yml, but if you ever need to change the volume, svm, or cluster at a site. All you have to do is change the values shown below.

#### vars/main.yml:
````
sites:
  cnj:
    cifs:
      prod:
        cluster: usarkntap01
        volume_name: "LABNAS05_PRD_CNJ_01"
        svm: usarkntap01svm1
      dev:
        cluster: usarkntap01
        volume_name: "LABNAS05_NONPRD_CNJ_01"
        svm: usarkntap01svm1
    nfs:
      prod:
        cluster: usarkntap01
        volume_name: "LABNAS05_PRD_CNJ_02"
        svm: usarkntap01svm1
      dev:
        cluster: usarkntap01
        volume_name: "LABNAS05_NONPRD_CNJ_02"
        svm: usarkntap01svm1
  adc:
    cifs:
      prod:
        cluster: usarkntap04
        volume_name: "LABNAS06_PRD_CNJ_01"
        svm: usarkntap04svm0
      dev:
        cluster: usarkntap04
        volume_name: "LABNAS06_NONPRD_CNJ_01"
        svm: usarkntap04svm0
    nfs:
      prod:
        cluster: usarkntap04
        volume_name: "LABNAS06_PRD_CNJ_02"
        svm: usarkntap04svm0
      dev:
        cluster: usarkntap04
        volume_name: "LABNAS06_NONPRD_CNJ_01"
        svm: usarkntap04svm0

                     
````

### Export Policy Rule Role Variables

*policy_rule_index*

   - This variable will set the index of the export policy rule

*policy_client_match:*

   - List of Client Match host names, IP Addresses, Netgroups, or Domains. If rule_index is not provided, client_match is used as a key to fetch current rule to determine create,delete,modify actions. If a rule with provided client_match exists, a new rule will not be created, but the existing rule will be modified or deleted. If a rule with provided client_match doesn't exist, a new rule will be created if state is present.

*policy_ro_rule* 

   - List of Read only access specifications for the rule (choices: any, none, never, krb5, krb5i, krb5p, ntlm, sys)
   
*policy_rw_rule:* 

   - List of Read Write access specifications for the rulen(choices: any, none, never, krb5, krb5i, krb5p, ntlm, sys)


### Qtree Role Variables

*qtree_name*

   - This variable will be set according to what is passed in for the share name inside the tower survey or extra vars ("qtree_{{ share_name }}")
   

### Quota Role Variables

*quota_type:*

   - The type of quota rule. Required to create or modify a rule.(choices: user, group, tree)
   


## **Role Initialization**


Before playbook execution, the required roles for this project must be downloaded from their respective repositories. The information for these roles is found in **roles/requirements.yml**

````
# => Ansible Role Requirements 
# => ONTAP API roles
- name: nyl-share-role
  src: https://jsingh05@bitbucket.org/jsingh05/nyl-share-role.git
  scm: git
  version: master

- name: nyl-export-policy-rule-role
  src: https://jsingh05@bitbucket.org/jsingh05/nyl-export-policy-rule-role.git
  scm: git
  version: master

- name: nyl-export-policy-role
  src: https://jsingh05@bitbucket.org/jsingh05/nyl-export-policy-role.git
  scm: git
  version: master

- name: nyl-qtree-role
  src: https://jsingh05@bitbucket.org/jsingh05/nyl-qtree-role.git
  scm: git
  version: master

- name: nyl-quota-role
  src: https://jsingh05@bitbucket.org/jsingh05/nyl-quota-role.git
  scm: git
  version: master
````

To configure this file, change the src variable to the URL for the specific repository locations for each role. Then run the following command and fill out the prompts that follow:

````
ansible-galaxy install -r roles/requirements.yml --force
````

After the installation of the roles, check for them in the /roles directory. If found, then this project is ready for playbook execution.

## **Running the Playbooks via Tower**

Set up your job template with your credentials and repo linked

Create a survey to pass in the extra vars needed for the build

Survey should include: Protocol type(cifs or nfs), site(cnj or adc), environment (prod or dev), export policy name, and share name. All the other optional variables described in each Role's README can be passed in as extra vars or optional survey questions.

After the survey is created and filled out, click launch


## **Running the Playbooks via CLI**

To run the playbooks for this project, the command line must specify the playbook and any extra variables that the user would have entered into the survey via ansible tower

The first section of the execution command will denote the specific playbook that is being used, as shown below

````
ansible-playbook nas.yml ....
````

Lastly, using the `-e` or `--extra-vars` flag, the user can override any variables that have been set for the playbook-execution. 

````
#cifs
ansible-playbook nas.yml  -e "object_type=cifs site=adc env=dev share_name=test_share export_policy_name=test_policy"

#nfs
ansible-playbook nas.yml  -e "object_type=nfs site=adc env=dev share_name=test_share"

*note that export_policy_name should only be added for nfs protocols, otherwise it will error out
````